﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ButtonClick : MonoBehaviour
{

    public void ButtonStart()
    {
        SceneManager.LoadScene("AccessScene");
    }

    public void ButtonExit()
    {
        Application.Quit();
    }
    
}
